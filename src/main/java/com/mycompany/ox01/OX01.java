/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ox01;

import java.util.Scanner;

/**
 *
 * @author Paweena Chinasri
 */
public class OX01 {
        public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = 3;
        char[][] board = new char[n][n];
        for (int i = 0; i<n; i++){
            for (int j = 0; j<n; j++){
                board[i][j] = '-';
            }
            System.out.println();
            
    }
         
    System.out.println("!!!WELCOME TO OX!!!");
    String m1 = "X";
    String m2 = "O";
    boolean player = true;
    boolean gameEnded = false;
while(!gameEnded){
    drawBoard(board);
    if(player){
        System.out.println("X " + "Turn");
    }else{
        System.out.println("O " + "Turn");
    }
    
    char z = '-';
    if(player){
        z = 'X';
    }else{
        z = 'O';
    }
    
    int num = 0;
    int row = 0;
    int col = 0;
    while(true){
        System.out.print("Please input your number:");num = kb.nextInt();
        if(num == 1){
            row = 0;
            col = 0;
        }if(num == 2){
            row = 0;
            col = 1;
        }if(num == 3){
            row = 0;
            col = 2;
        }if(num == 4){
            row = 1;
            col = 0;
        }if(num == 5){
            row = 1;
            col = 1;
        }if(num == 6){
            row = 1;
            col = 2;
        }if(num == 7){
            row = 2;
            col = 0;
        }if(num == 8){
            row = 2;
            col = 1;
        }if(num == 9){
            row = 2;
            col = 2;
        }
        if(num>9){
            System.out.println("There is no position on this board");
        }else if(board[row][col] != '-'){
            System.out.println("This positionn has already been used by a player");
        }else{
            break;}
        }
    board[row][col] = z;
    if(playerHasWon(board) == 'X'){
        System.out.println("!Congratulations!");
        System.out.println("X " + "Win!!!");
        System.out.println("Continue (Y/N)");
        gameEnded = true;
    }else if(playerHasWon(board) == 'O'){
        System.out.println("!Congratulations!");
        System.out.println("O " + "Win!!!");
        System.out.println("Continue (Y/N)");
        gameEnded = true;
    }else {
        if(boardIsFull(board)){
            System.out.println("!It's a Tie!");
            gameEnded = true;    
    }else
        {player = !player;}
   }
}
drawBoard(board);
}
public static void drawBoard(char[][] board){
System.out.println("Your Board:");
for(int i = 0; i<board.length; i++){
    for(int j = 0; j<board[i].length; j++){
        System.out.print(board[i][j]);
    }
    System.out.println();
    }
}

public static char playerHasWon(char[][] board){
    for(int i =0; i<board.length; i++){
        boolean inARow = true;
        char value = board[i][0];
    if(value == '-'){
        inARow = false;
    }else{
        for(int j =1; j<board[i].length; j++){
            if(board[i][j] != value){
                inARow = false;
                break;}
            }
        }
    
    if(inARow){
        return value;
    }   
    }
    
    for(int j = 0; j<board[0].length; j++){
    boolean inACol = true;
    char value = board[0][j];
    
    if(value == '-'){
    inACol = false;
    }else{
    for(int i = 1; i<board.length; i++){
    if(board[i][j] != value){
    inACol = false;
    break;
    }
    }
    }
    
    if(inACol){
    return value;
    }
    }
    
    boolean inADiag1 = true;
    char value1 = board[0][0];
    if(value1 == '-'){
    inADiag1 = false;
    }else{
    for(int i = 1; i<board.length; i++){
    if(board[i][i] != value1){
    inADiag1 = false;
    break;
    }
    }
    }
    
    if(inADiag1){
    return value1;
    }
    
    boolean inADiag2 = true;
    char value2 = board[0][board.length-1];
    
    if(value2 == '-'){
    inADiag2 = false;
    }else{
    for(int i = 1; i<board.length; i++){
    if(board[i][board.length-1-i] != value2){
    inADiag2 = false;
    break;
    }
    }
    }
    
    if(inADiag2){
    return value2;
    }
    
    return ' ';
    }


    public static boolean boardIsFull(char[][] board){
    for(int i = 0; i<board.length; i++){
    for(int j = 0; j<board[i].length; j++){
    if(board[i][j] == '-'){
    return false;
    }
    }
    }
    return true;
}
     
    }

